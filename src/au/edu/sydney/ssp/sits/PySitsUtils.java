package au.edu.sydney.ssp.sits;

/**
 *
 * Copyright 2016 University of Sydney
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.servlet.ServletContext;

public class PySitsUtils {

	private static final int BYTES_DOWNLOAD = 1024;

	public static String getProjectList(ServletContext context) {
		File tmpFile;
		try {
			tmpFile = File.createTempFile("project-list-", ".txt");
		} catch (IOException iox) {
			iox.printStackTrace();
			return "";
		}

		String script = context.getRealPath("/WEB-INF/scripts/project-list.sh");
		String[] cmd = { "bash", script, tmpFile.getAbsolutePath() };
		runCommand(cmd);

		String curLine = null;
		BufferedReader br = null;
		String string_base = "<li><a href='export/%s'>%s</a></li>";
		StringBuffer sb = new StringBuffer();

		try {
			br = new BufferedReader(new FileReader(tmpFile));
			while ((curLine = br.readLine()) != null) {
				sb.append(String.format(string_base, curLine, curLine));
			}
			br.close();
		} catch (IOException iox) {
			iox.printStackTrace();
			return "";
		} finally {
			tmpFile.delete();
		}

		return sb.toString();
	}

	public static void printProjectXml(String project, ServletContext context, OutputStream os) {
		File tmpFile;
		String filename = project == null ? "" : project.replaceAll("[^A-Za-z0-9 _]",  "");
		try {
			tmpFile = File.createTempFile(filename + "-", ".xml");
		} catch (IOException iox) {
			iox.printStackTrace();
			return;
		}

		String script = context.getRealPath("/WEB-INF/scripts/project-export.sh");
		String[] cmd = { "bash", script, project, tmpFile.getAbsolutePath() };
		runCommand(cmd);

		int read = 0;
		byte[] bytes = new byte[BYTES_DOWNLOAD];
		try {
			InputStream is = new FileInputStream(tmpFile);

			while ((read = is.read(bytes)) != -1) {
				os.write(bytes, 0, read);
			}
			is.close();
			os.flush();
		} catch (FileNotFoundException fnfx) {
			fnfx.printStackTrace();
		} catch (IOException iox) {
			iox.printStackTrace();
		} finally {
			tmpFile.delete();
		}
	}

	public static void runCommand(String[] command) {
		Process p = null;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
		} catch (IOException iox) {
			iox.printStackTrace();
		} catch (InterruptedException ix) {
			ix.printStackTrace();
		}
	}

	public static void printProcessOutput(Process p) throws IOException {
		String line;
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		while ((line = input.readLine()) != null) {
			System.out.println(line);
		}
		input.close();

		BufferedReader output = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		while ((line = output.readLine()) != null) {
			System.out.println(line);
		}
		output.close();
	}
}
